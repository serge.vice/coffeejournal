/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sergeostrianyn
 */
public class TasteNote {
    private String body;
    private String grainRef;
    private double mark;

    public TasteNote(String body, String grainIdentifierReference, double mark) {
        this.body = body;
        this.grainRef = grainIdentifierReference;
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "TasteNote{" + "body=" + body.substring(0, 10) + ", grainRef=" + grainRef + ", mark=" + mark + '}';
    }
}
