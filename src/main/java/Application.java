
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author sergeostrianyn
 */
public class Application {

    public static void main(String[] args) {
        String importgrainsCommand = "importgrains";
        String importnotesCommand = "importnotes";
        String endCommand = "end";

        System.out.println("Hello. I'm a CoffeeJournal");
        System.out.println("type " + importgrainsCommand + " to import grains");
        System.out.println("type " + importnotesCommand + " to import notes");
        System.out.println("type " + endCommand + " to end");
        System.out.println("");
        Scanner scanner = new Scanner(System.in);
        String input;
        
        while (true) {
            input = scanner.nextLine();
            
            if (importgrainsCommand.equals(input)) {
                //System.out.println("file path?");
                //String filePath = scanner.nextLine();
                String filePath = "/Users/sergeostrianyn/Desktop/PetProjects/CoffeeJournal/src/main/resources/Grains 446c20eb3c004887aaffe4d7b4f9ad63.csv";
                List<Grain> grains = importGrainsFromFile(filePath);
                grains.stream().forEach(n -> System.out.println(n));
            }
            
            if (importnotesCommand.equals(input)) {
                //System.out.println("file path?");
                //String filePath = scanner.nextLine();
                String filePath = "/Users/sergeostrianyn/Desktop/PetProjects/CoffeeJournal/src/main/resources/Taste Notes 62f2ae2c40064151a96e3500007218f0.csv";
                List<TasteNote> notes = importTasteNotesFromFile(filePath);
                notes.stream().forEach(n -> System.out.println(n));
            }
            
            if (endCommand.equals(input)) {
                break;
            }
        }

    }

    public static List<Grain> importGrainsFromFile(String file) {
        List<Grain> grains = new ArrayList<>();

        try {
            grains = Files.lines(Paths.get(file))
                    .map(line -> line.split(","))
                    .map(line -> new Grain(line[line.length - 1], line[0]))
                    .collect(Collectors.toCollection(ArrayList::new));
        } catch (Exception e) {
            System.out.println("file '" + file + "' not found");
        }

        grains.remove(0);
        return grains;
    }

    public static List<TasteNote> importTasteNotesFromFile(String file) {
        List<TasteNote> tasteNotes = new ArrayList<>();

        try {
            tasteNotes = Files.lines(Paths.get(file))
                    .map(line -> line.split(","))
                    .filter(line -> !line[3].isBlank())
                    .map(line -> new TasteNote(line[1], getIdentifier(line[3], "-"), Double.valueOf(line[4])))
                    .collect(Collectors.toCollection(ArrayList::new));
        } catch (Exception e) {
            e.printStackTrace();
        }

        //tasteNotes.remove(0);
        return tasteNotes;
    }

    public static String getIdentifier(String string, String split) {
        String[] substrings = string.split(split);
        return substrings[substrings.length - 1];
    }
}
