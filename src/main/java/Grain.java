/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sergeostrianyn
 */
public class Grain {
    private String identifier;
    private String name;

    public String getName() {
        return name;
    }

    public Grain(String identifier, String name) {
        this.identifier = identifier;
        this.name = name;
    }

    public String getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        return "Grain{" + "identifier=" + identifier + ", name=" + name + '}';
    }
}
